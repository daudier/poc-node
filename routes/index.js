var express = require('express');
var router = express.Router();

/* GET all pages. */
router.get('*', function(req, res) {
  res.render('index', {title: 'Road Scholar'});
});

module.exports = router;
