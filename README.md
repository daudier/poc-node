# Road Scholar POC

### Steps to Get the App Running
1. Install **Node** (instructions [here](http://nodejs.org)).
2. Fork this repo.
3. Clone it down locally.
4. In your terminal, type `npm i` to install all the modules you need.
5. In your terminal, type `npm start` to start the server.
6. In your terminal, type `npm install webpack -g` so you can use the webpack CLI.
7. In your terminal, type `webpack  --progress --colors --watch`. Webpack is a module loader and will watch for any client-side code changes and regenerate the modules.

### Client-Side Libs
* Backbone
* Marionette
* [6to5](https://github.com/sebmck/6to5) - turns ES6 code into readable vanilla ES5 with source maps
* jQuery
* Lo-Dash
* Stylus
* [Webpack](http://webpack.github.io)

### Server-Side Tech
* Node
* Express
* Jade
* [Parse](https://parse.com)

![img](https://getdrip.s3.amazonaws.com/uploads/image_upload/image/3314/embeddable_44acde4e-3999-48e0-b187-4762a8d1f75b.png)