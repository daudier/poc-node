var webpack = require('webpack');
var path    = require('path');

//var commonsPlugin = new webpack.optimize.CommonsChunkPlugin('common.js');

module.exports = {
	entry: {
		main: './src/rs-app'
	},
	output: {
		path: path.join(__dirname, 'public'),
		filename: '[name].js' // Template based on keys in entry above
		//chunkFilename: '[name].[hash].js'
	},
	//plugins: [commonsPlugin],
	module: {
		loaders: [
			{ test: /\.js$/, exclude: /node_modules/, loader: 'babel-loader'},
			{ test: /\.jade$/, loader: 'jade' },
			{ test: /\.styl$/, loader: 'style-loader!css-loader!stylus-loader' },
			{ test: /\.(png|jpg)$/, loader: 'url-loader?limit=8192'} // inline base64 URLs for <=8k images, direct URLs for the rest
		]
	},
	resolve: {
		extensions: ['', '.js', '.styl']
	},
	plugins: [
		new webpack.ProvidePlugin({ // Make modules available as free variables inside modules
			$:          'jquery',
			jQuery:     'jquery',
			_:          'lodash',
			Backbone:   'backbone',
			Marionette: 'backbone.marionette',
			Radio: 			'backbone.radio',
			BaseRouter: 'backbone.base-router'
		})
	],
	node: {
		fs: 'empty'
	}
};