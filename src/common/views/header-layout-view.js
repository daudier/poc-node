var headerTpl = require('./templates/header-layout-view.jade');

class HeaderLayoutView extends Marionette.LayoutView {
	constructor() {
		this.template = headerTpl;

		this.className = 'wrapper';

		this.ui = {
			'signInBtn': '[data-action=sign-in]'
		};

		this.events = {
			'click @ui.signInBtn': '_handleSignIn'
		};

		this._handleSignIn = function(e) {
			e.preventDefault();
			
			console.log('handle login')
		};

		super();
	}
}

export default HeaderLayoutView;

