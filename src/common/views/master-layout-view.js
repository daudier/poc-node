class MasterLayoutView extends Marionette.LayoutView {
	constructor() {
		this.el = 'body';

		this.template = '#master-layout-tpl';

		this.ui = {
			header: 'header',
			main: 'main',
			footer: 'footer'
		};

		this.regions = {
			header: '@ui.header',
			main: '@ui.main',
			footer: '@ui.footer'
		};

		super();
	}
}

export default MasterLayoutView;