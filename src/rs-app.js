import Router from './core/router';
import MasterLayoutView from './common/views/master-layout-view';
import HeaderLayoutView from './common/views/header-layout-view';

var app = new Marionette.Application();
new Router();

app.on('start', function() {
  Parse.initialize('9MfwkG2a5zlNhdg01NXHsMkoKudj1GDMupixgTF0', 'mPXPIUKRu7M3oC1H5ecQdmhPTvFn03TfgoBa6SFD');

  var masterLayoutView = new MasterLayoutView();
  masterLayoutView.render();

  var headerLayoutView = new HeaderLayoutView();
  masterLayoutView.getRegion('header').show(headerLayoutView);

  Backbone.history.start({
    pushState: true
  });

  Radio.DEBUG = true;

  console.log('Road Scholar is running!');
});

app.start();

export default app;