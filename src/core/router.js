class Router extends BaseRouter {
	constructor() {
		//this.initialize = function() {
		//	Radio.comply('router', 'navigate', function(route) {
		//		this.navigate(route, {trigger:true});
		//	}, this);
		//};

		this.onNavigate = function(routeData) {
			var RouteObj = routeData.linked;
			RouteObj.show();
		};

		this.routes = {
			'(/)': {
				show: function() {
					require.ensure([], function() {
						require('../modules/home/home-route'); // when this function is called, the module is guaranteed to be synchronously available.
					});
				}
			},
			'my-account': {
				show: function() {
					require.ensure([], function() {
						require('../modules/my-account/my-account-route');
					});
				}
			}
		};

		super();
	}
}

export default Router;